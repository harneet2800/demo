package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;
@Service
public interface UserService {
	void save(User user);

    public User findByUsername(String username);
    public User findByUsernameAndPassword(String username, String password);
    
    void autoLogin(String username, String password);
}
