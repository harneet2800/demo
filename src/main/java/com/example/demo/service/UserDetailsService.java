package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserDetailsService {
    
	@Autowired
    private UserRepository userRepository;

    @Transactional
    public User loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }
    
    @Transactional
    public User loadUserByUsernameAndPassword(String username, String password) {
        User user = userRepository.findByUsernameAndPassword(username,password);
        return user;
    }
}
