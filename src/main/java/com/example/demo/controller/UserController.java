package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.UserValidator;
import com.example.demo.model.User;
import com.example.demo.service.UserService;

@Controller
public class UserController {

	@Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;
    
	@GetMapping("/registration")
    public String registration(Model model) {
		/*
		 * if ("a" == "a") { return "redirect:/"; }
		 */
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        userService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login( User userForm, String error, String logout) {
    	User user = userService.findByUsernameAndPassword(userForm.getUsername(), userForm.getPassword()); 
    	 return "redirect:/welcome";
		 

		/*
		 * if (error != null) model.addAttribute("error",
		 * "Your username and password is invalid.");
		 * 
		 * if (logout != null) model.addAttribute("message",
		 * "You have been logged out successfully.");
		 */
        //return "login";
    }
    
    @GetMapping({"/", "/welcome"})
    public String welcome(Model model) {
        return "welcome";
    }
}
