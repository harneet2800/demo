package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.User;

@Controller
public class HelloWorldController {

	@RequestMapping("/")
	public String hello(Model model) {
		model.addAttribute("name", "Harneet K");
		model.addAttribute("class", "PGDCA");
		return "hello";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}
	
	/*
	 * @RequestMapping("/registration") public String registration(Model model) {
	 * model.addAttribute("userForm", new User()); return "registration"; }
	 */
}
